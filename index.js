let express = require("express");
const PORT = 4000;
let app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));



let url = "/hello";
app.get(url, (req,res) => res.send(`Hello from the ${url} endpoint`));

app.post("/greeting", (req,res) => {
	console.log(req.body);

	 res.send(`Hello there ${req.body.firstName}!`);

} );

app.listen(PORT, () => {
	console.log(`Server is now connected to port ${PORT}.`)

} );